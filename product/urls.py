from django.urls import path
from product.views import *

urlpatterns = [
    path('', product_main_page, name='product_main'),
    path('category-cr', CreateCategoryView.as_view(), name='category_create'),
    path('product-cr', CreateProductView.as_view(), name='product_create'),
    #path('stock-add', add_stock, name='stock_add'),
    #path('stock-sub', subtract_stock, name='stock_sub'),
]