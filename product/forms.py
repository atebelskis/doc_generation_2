from django.forms import ModelForm
from product.models import Category, Product, Stock


class CreateCategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = '__all__'

class CreateProductForm(ModelForm):
    class Meta:
        model = Product
        fields = '__all__'