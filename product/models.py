from django.db import models


class Category (models.Model):
    cat_id = models.AutoField(primary_key=True)
    cat_name = models.CharField(max_length=50, unique=True, verbose_name="Kategorijos pavadinimas")

    class Meta:
        db_table = "Category"

    def __str__(self):
        return self.cat_name


class Product (models.Model):
    product_id = models.AutoField(primary_key=True)
    product_name = models.CharField(max_length=50, verbose_name='Prekės pavadinimas', unique=True)
    cat = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Kategorija')

    class Meta:
        db_table = "Product"

    def __str__(self):
        return self.product_name


class Stock (models.Model):
    stock_id = models.AutoField(primary_key=True)
    date = models.DateField(verbose_name="Data")
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(verbose_name="Kiekis")

    class Meta:
        db_table = "Stock"

    def __str__(self):
        return f'{self.date}'

