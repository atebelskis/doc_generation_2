from django.shortcuts import render
from django.views.generic import CreateView
from product.models import Category, Product
from product.forms import CreateProductForm, CreateCategoryForm

def product_main_page(request):
    return render(request, 'product.html')

class CreateCategoryView(CreateView):
    model = Category
    form_class = CreateCategoryForm
    template_name = 'category_create.html'
    success_url = '/'

class CreateProductView(CreateView):
    model = Product
    form_class = CreateProductForm
    template_name = 'product_create.html'
    success_url = '/'

# def add_stock():
#     pass
#
# def subtract_stock():
#     pass
