from django.db import models
from product.models import Product



class Customer(models.Model):
    customer_name = models.CharField(max_length=50, verbose_name="Užsakovo pavadinimas", unique=True)
    registration_date = models.DateField(verbose_name="Registracijos data")
    customer_id = models.AutoField(primary_key=True)

    class Meta:
        db_table = "Customer"

    def __str__(self):
        return self.customer_name


class Order(models.Model):
    order_id = models.AutoField(primary_key=True)
    date = models.DateField(verbose_name="Data", unique=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    class Meta:
        db_table = "Order"

    def __str__(self):
        return f'{self.date}'


class OrderLine(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    order_lines_id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    price = models.FloatField(verbose_name="Kaina")
    quantity = models.IntegerField(verbose_name="Kiekis")

    class Meta:
        db_table = "OrderLine"

