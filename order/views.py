from django.shortcuts import render
from django.views.generic import CreateView
from product.models import Category, Product
from order.models import Order, OrderLine, Customer
from order.forms import CreateOrderForm, CreateOrderLineForm, CreateCustomerForm

def order_main_page(request):
    return render(request, 'order.html')

class CreateOrderView(CreateView):
    model = Order
    form_class = CreateOrderForm
    template_name = 'order_create.html'
    success_url = '/'

class CreateOrderLineView(CreateView):
    model = OrderLine
    form_class = CreateOrderLineForm
    template_name = 'orderline_create.html'
    success_url = '/'

class CreateCustomerView(CreateView):
    model = Customer
    form_class = CreateCustomerForm
    template_name = 'customer_create.html'
    success_url = '/order/'
