from django.contrib import admin
from .models import *

admin.site.register(OrderLine)
admin.site.register(Order)
admin.site.register(Customer)
