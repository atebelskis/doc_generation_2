from django.urls import path
from order.views import *

urlpatterns = [
    path('', order_main_page, name='order_main'),
    path('order-cr', CreateOrderView.as_view(), name='order_create'),
    path('order-line-cr', CreateOrderLineView.as_view(), name='orderline_create'),
    path('customer-cr', CreateCustomerView.as_view(), name='customer_create'),
]