from django.forms import ModelForm
from order.models import Order, OrderLine, Customer


class CreateOrderForm(ModelForm):
    class Meta:
        model = Order
        fields = '__all__'

class CreateOrderLineForm(ModelForm):
    class Meta:
        model = OrderLine
        fields = '__all__'

class CreateCustomerForm(ModelForm):
    class Meta:
        model = Customer
        fields = '__all__'