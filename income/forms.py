from django.forms import ModelForm
from income.models import Income, IncomeProduct


class CreateIncomeForm(ModelForm):
    class Meta:
        model = Income
        fields = '__all__'

class CreateIncomeProductForm(ModelForm):
    class Meta:
        model = IncomeProduct
        fields = '__all__'