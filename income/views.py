from django.shortcuts import render
from django.views.generic import CreateView
from income.models import Income, IncomeProduct
from product.models import Category, Product
from income.forms import CreateIncomeForm, CreateIncomeProductForm

def income_main_page(request):
    return render(request, 'income.html')

class CreateIncomeView(CreateView):
    model = Income
    form_class = CreateIncomeForm
    template_name = 'income_create.html'
    success_url = '/'

class CreateIncomeProductView(CreateView):
    model = IncomeProduct
    form_class = CreateIncomeProductForm
    template_name = 'income_product_create.html'
    success_url = '/'

