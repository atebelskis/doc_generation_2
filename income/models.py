from django.db import models
from product.models import Product

class Income (models.Model):
    income_id = models.AutoField(primary_key=True)
    date = models.DateField(verbose_name="Data", unique=True)

    class Meta:
        db_table = "Income"

    def __str__(self):
        return f'{self.date}'


class IncomeProduct(models.Model):
    income_product_id = models.AutoField(primary_key=True)
    income = models.ForeignKey(Income, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(verbose_name="Kiekis")

    class Meta:
        db_table = "IncomeProduct"


