from django.urls import path
from income.views import *

urlpatterns = [
    path('', income_main_page, name='income_main'),
    path('income-cr/', CreateIncomeView.as_view(), name='income_create'),
    path('income-product-cr/', CreateIncomeProductView.as_view(), name='income_product_create')
]