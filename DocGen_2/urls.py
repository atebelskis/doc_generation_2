
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('', include('core.urls'), name='core'),
    path('income/', include('income.urls')),
    path('product/', include('product.urls')),
    path('order/', include('order.urls')),
]
